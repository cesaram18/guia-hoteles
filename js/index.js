    $(function(){
        $("[data-toggle='tooltip']").tooltip();
        $("[data-toggle='popover']").popover();
        $('.carousel').carousel({
          interval:1500
        });

        $('#comprar').on('show.bs.modal', function(e){
            console.log('El modal registro se está mostrando');

            $('#buttonCompra').removeClass('btn-outline-success');
            $('#buttonCompra').addClass('btn-primary');

        });
        $('#comprar').on('shown.bs.modal', function(e){
            console.log('El modal registro se mostró');
        });
        $('#comprar').on('hide.bs.modal', function(e){
            console.log('El modal registro se está ocultando');
        });
        $('#comprar').on('hidden.bs.modal', function(e){
            console.log('El modal registro se ocultó');

            $('#buttonCompra').addClass('btn btn-outline-success');

          });
        
    });
    